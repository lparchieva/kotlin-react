'use strict';

var webpack = require('webpack');

var config = {
    "mode": "development",
    "context": "/Users/lemaausheva/Desktop/IntelIDEA/IdeaProjects/test/build/kotlin-js-min/main",
    "entry": {
        "main": "./test"
    },
    "output": {
        "path": "/Users/lemaausheva/Desktop/IntelIDEA/IdeaProjects/test/build/bundle",
        "filename": "[name].bundle.js",
        "chunkFilename": "[id].bundle.js",
        "publicPath": "/"
    },
    "module": {
        "rules": [
            
        ]
    },
    "resolve": {
        "modules": [
            "kotlin-js-min/main",
            "resources/main",
            "/Users/lemaausheva/Desktop/IntelIDEA/IdeaProjects/test/build/node_modules",
            "node_modules"
        ]
    },
    "plugins": [
        
    ]
};

module.exports = config;

