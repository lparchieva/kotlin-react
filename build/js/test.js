(function (_, Kotlin, $module$kotlin_react, $module$kotlin_react_dom, $module$kotlinx_html_js) {
  'use strict';
  var $$importsForInline$$ = _.$$importsForInline$$ || (_.$$importsForInline$$ = {});
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var getKClass = Kotlin.getKClass;
  var Unit = Kotlin.kotlin.Unit;
  var RComponent_init = $module$kotlin_react.react.RComponent_init_lqgejo$;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var RComponent = $module$kotlin_react.react.RComponent;
  var Kind_INTERFACE = Kotlin.Kind.INTERFACE;
  var mutableListOf = Kotlin.kotlin.collections.mutableListOf_i5x0yv$;
  var setState = $module$kotlin_react.react.setState_jbmi3x$;
  var getCallableRef = Kotlin.getCallableRef;
  var set_onClickFunction = $module$kotlinx_html_js.kotlinx.html.js.set_onClickFunction_pszlq2$;
  var InputType = $module$kotlinx_html_js.kotlinx.html.InputType;
  var set_onChangeFunction = $module$kotlinx_html_js.kotlinx.html.js.set_onChangeFunction_pszlq2$;
  var equals = Kotlin.equals;
  var throwCCE = Kotlin.throwCCE;
  var setOf = Kotlin.kotlin.collections.setOf_mh5how$;
  var set_classes = $module$kotlinx_html_js.kotlinx.html.set_classes_njy09m$;
  var render = $module$kotlin_react_dom.react.dom.render_4s0l5f$;
  App.prototype = Object.create(RComponent.prototype);
  App.prototype.constructor = App;
  HomeView.prototype = Object.create(RComponent.prototype);
  HomeView.prototype.constructor = HomeView;
  NewTaskComponent.prototype = Object.create(RComponent.prototype);
  NewTaskComponent.prototype.constructor = NewTaskComponent;
  ToDoList.prototype = Object.create(RComponent.prototype);
  ToDoList.prototype.constructor = ToDoList;
  function App() {
    App$Companion_getInstance();
    RComponent_init(this);
  }
  function App$Companion() {
    App$Companion_instance = this;
    this.TO_DO_APP = 'TO_DO_APP';
  }
  App$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var App$Companion_instance = null;
  function App$Companion_getInstance() {
    if (App$Companion_instance === null) {
      new App$Companion();
    }
    return App$Companion_instance;
  }
  function App$render$lambda($receiver) {
    return Unit;
  }
  App.prototype.render_ss14n$ = function ($receiver) {
    $receiver.child_drr62v$(getKClass(HomeView), App$render$lambda);
  };
  App.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'App',
    interfaces: [RComponent]
  };
  function HomeViewState() {
  }
  HomeViewState.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'HomeViewState',
    interfaces: []
  };
  function HomeView() {
    RComponent_init(this);
  }
  HomeView.prototype.init_bc6fkx$ = function ($receiver) {
    $receiver.loading = false;
    $receiver.taskList = mutableListOf([new Task('\u041F\u0440\u0438\u043C\u0435\u0440', '\u041E\u043F\u0438\u0441\u0430\u043D\u0438\u0435')]);
  };
  function HomeView$closeModal$lambda($receiver) {
    $receiver.createModalOpened = false;
    return Unit;
  }
  HomeView.prototype.closeModal_0 = function () {
    setState(this, HomeView$closeModal$lambda);
  };
  function HomeView$addToTaskList$lambda(closure$newTaskList) {
    return function ($receiver) {
      $receiver.taskList = closure$newTaskList;
      return Unit;
    };
  }
  HomeView.prototype.addToTaskList_0 = function (task) {
    var newTaskList = this.state.taskList;
    newTaskList.add_11rb$(task);
    setState(this, HomeView$addToTaskList$lambda(newTaskList));
  };
  function HomeView$checkTask$lambda(closure$newTaskList) {
    return function ($receiver) {
      $receiver.taskList = closure$newTaskList;
      return Unit;
    };
  }
  HomeView.prototype.checkTask_0 = function (task, checked) {
    var tmp$;
    var newTaskList = this.state.taskList;
    var firstOrNull$result;
    firstOrNull$break: do {
      var tmp$_0;
      tmp$_0 = newTaskList.iterator();
      while (tmp$_0.hasNext()) {
        var element = tmp$_0.next();
        if (task != null ? task.equals(element) : null) {
          firstOrNull$result = element;
          break firstOrNull$break;
        }
      }
      firstOrNull$result = null;
    }
     while (false);
    if ((tmp$ = firstOrNull$result) != null) {
      tmp$.isCompleted = checked;
    }
    setState(this, HomeView$checkTask$lambda(newTaskList));
  };
  var attributesMapOf = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.attributesMapOf_jyasbz$;
  var H1_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.H1;
  function h1$lambda(closure$classes) {
    return function (it) {
      return new H1_init(attributesMapOf('class', closure$classes), it);
    };
  }
  var RDOMBuilder_init = $module$kotlin_react_dom.react.dom.RDOMBuilder;
  function HomeView$render$lambda$lambda$lambda$lambda(this$HomeView) {
    return function ($receiver) {
      $receiver.attrs.taskList = this$HomeView.state.taskList;
      $receiver.attrs.checkTask = getCallableRef('checkTask', function ($receiver, task, checked) {
        return $receiver.checkTask_0(task, checked), Unit;
      }.bind(null, this$HomeView));
      return Unit;
    };
  }
  function HomeView$render$lambda$lambda$lambda$lambda$lambda$lambda($receiver) {
    $receiver.createModalOpened = true;
    return Unit;
  }
  function HomeView$render$lambda$lambda$lambda$lambda$lambda(this$HomeView) {
    return function (it) {
      setState(this$HomeView, HomeView$render$lambda$lambda$lambda$lambda$lambda$lambda);
      return Unit;
    };
  }
  var enumEncode = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.attributes.enumEncode_m4whry$;
  var attributesMapOf_0 = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.attributesMapOf_alerag$;
  var BUTTON_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.BUTTON;
  function button$lambda(closure$formEncType, closure$formMethod, closure$type, closure$classes) {
    return function (it) {
      return new BUTTON_init(attributesMapOf_0(['formenctype', closure$formEncType != null ? enumEncode(closure$formEncType) : null, 'formmethod', closure$formMethod != null ? enumEncode(closure$formMethod) : null, 'type', closure$type != null ? enumEncode(closure$type) : null, 'class', closure$classes]), it);
    };
  }
  var P_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.P;
  function p$lambda(closure$classes) {
    return function (it) {
      return new P_init(attributesMapOf('class', closure$classes), it);
    };
  }
  var DIV_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.DIV;
  function div$lambda(closure$classes) {
    return function (it) {
      return new DIV_init(attributesMapOf('class', closure$classes), it);
    };
  }
  var ASIDE_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.ASIDE;
  function aside$lambda(closure$classes) {
    return function (it) {
      return new ASIDE_init(attributesMapOf('class', closure$classes), it);
    };
  }
  function HomeView$render$lambda$lambda$lambda(this$HomeView) {
    return function () {
      this$HomeView.closeModal_0();
      return Unit;
    };
  }
  function HomeView$render$lambda$lambda(this$HomeView) {
    return function ($receiver) {
      $receiver.attrs.onAdd = HomeView$render$lambda$lambda$lambda(this$HomeView);
      $receiver.attrs.Add = getCallableRef('addToTaskList', function ($receiver, task) {
        return $receiver.addToTaskList_0(task), Unit;
      }.bind(null, this$HomeView));
      return Unit;
    };
  }
  var HEADER_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.HEADER;
  function header$lambda(closure$classes) {
    return function (it) {
      return new HEADER_init(attributesMapOf('class', closure$classes), it);
    };
  }
  HomeView.prototype.render_ss14n$ = function ($receiver) {
    var $receiver_0 = new RDOMBuilder_init(div$lambda(null));
    var $receiver_0_0 = new RDOMBuilder_init(header$lambda('header'));
    var $receiver_0_1 = new RDOMBuilder_init(h1$lambda('layout header__title'));
    $receiver_0_1.unaryPlus_pdl1vz$('Simple React + Kotlin TODO App');
    $receiver_0_0.child_2usv9w$($receiver_0_1.create());
    $receiver_0.child_2usv9w$($receiver_0_0.create());
    var $receiver_0_2 = new RDOMBuilder_init(div$lambda('main layout'));
    if (this.state.loading) {
      var $receiver_0_3 = new RDOMBuilder_init(p$lambda(null));
      $receiver_0_3.unaryPlus_pdl1vz$('Loading');
      $receiver_0_2.child_2usv9w$($receiver_0_3.create());
    }
     else {
      var $receiver_0_4 = new RDOMBuilder_init(div$lambda('main__list'));
      $receiver_0_4.child_drr62v$(getKClass(ToDoList), HomeView$render$lambda$lambda$lambda$lambda(this));
      $receiver_0_2.child_2usv9w$($receiver_0_4.create());
    }
    var $receiver_0_5 = new RDOMBuilder_init(aside$lambda('main__controls'));
    var $receiver_0_6 = new RDOMBuilder_init(button$lambda(null, null, null, 'main__button'));
    $receiver_0_6.unaryPlus_pdl1vz$('ADD');
    set_onClickFunction($receiver_0_6.attrs, HomeView$render$lambda$lambda$lambda$lambda$lambda(this));
    $receiver_0_5.child_2usv9w$($receiver_0_6.create());
    $receiver_0_2.child_2usv9w$($receiver_0_5.create());
    $receiver_0.child_2usv9w$($receiver_0_2.create());
    if (this.state.createModalOpened) {
      $receiver_0.child_drr62v$(getKClass(NewTaskComponent), HomeView$render$lambda$lambda(this));
    }
    $receiver.child_2usv9w$($receiver_0.create());
  };
  HomeView.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HomeView',
    interfaces: [RComponent]
  };
  function NewTaskComponentProps() {
  }
  NewTaskComponentProps.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'NewTaskComponentProps',
    interfaces: []
  };
  function NewTaskComponentState() {
  }
  NewTaskComponentState.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'NewTaskComponentState',
    interfaces: []
  };
  function NewTaskComponent() {
    RComponent_init(this);
  }
  NewTaskComponent.prototype.init_bc6fkx$ = function ($receiver) {
    $receiver.title = '';
    $receiver.description = '';
    $receiver.error = false;
  };
  var INPUT_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.INPUT;
  function input$lambda(closure$type, closure$formEncType, closure$formMethod, closure$name, closure$classes) {
    return function (it) {
      return new INPUT_init(attributesMapOf_0(['type', closure$type != null ? enumEncode(closure$type) : null, 'formenctype', closure$formEncType != null ? enumEncode(closure$formEncType) : null, 'formmethod', closure$formMethod != null ? enumEncode(closure$formMethod) : null, 'name', closure$name, 'class', closure$classes]), it);
    };
  }
  function NewTaskComponent$render$lambda$lambda$lambda$lambda$lambda$lambda($receiver) {
    $receiver.title = '';
    $receiver.description = '';
    return Unit;
  }
  function NewTaskComponent$render$lambda$lambda$lambda$lambda$lambda(this$NewTaskComponent) {
    return function (it) {
      this$NewTaskComponent.props.Add(new Task(this$NewTaskComponent.state.title, this$NewTaskComponent.state.description));
      setState(this$NewTaskComponent, NewTaskComponent$render$lambda$lambda$lambda$lambda$lambda$lambda);
      this$NewTaskComponent.props.onAdd();
      return Unit;
    };
  }
  function NewTaskComponent$render$lambda$lambda$lambda$lambda$lambda_0(this$NewTaskComponent) {
    return function (it) {
      this$NewTaskComponent.props.onAdd();
      return Unit;
    };
  }
  function button$lambda_0(closure$formEncType, closure$formMethod, closure$type, closure$classes) {
    return function (it) {
      return new BUTTON_init(attributesMapOf_0(['formenctype', closure$formEncType != null ? enumEncode(closure$formEncType) : null, 'formmethod', closure$formMethod != null ? enumEncode(closure$formMethod) : null, 'type', closure$type != null ? enumEncode(closure$type) : null, 'class', closure$classes]), it);
    };
  }
  function h1$lambda_0(closure$classes) {
    return function (it) {
      return new H1_init(attributesMapOf('class', closure$classes), it);
    };
  }
  function div$lambda_0(closure$classes) {
    return function (it) {
      return new DIV_init(attributesMapOf('class', closure$classes), it);
    };
  }
  function p$lambda_0(closure$classes) {
    return function (it) {
      return new P_init(attributesMapOf('class', closure$classes), it);
    };
  }
  NewTaskComponent.prototype.render_ss14n$ = function ($receiver) {
    var $receiver_0 = new RDOMBuilder_init(div$lambda_0('modal'));
    var $receiver_0_0 = new RDOMBuilder_init(div$lambda_0('modal__content'));
    var $receiver_0_1 = new RDOMBuilder_init(h1$lambda_0(null));
    $receiver_0_1.unaryPlus_pdl1vz$('\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u0434\u0435\u043B\u043E');
    $receiver_0_0.child_2usv9w$($receiver_0_1.create());
    var $receiver_0_2 = new RDOMBuilder_init(div$lambda_0('modal__input-list'));
    var $receiver_0_3 = new RDOMBuilder_init(input$lambda(null, null, null, null, 'modal__input'));
    $receiver_0_3.attrs.type = InputType.text;
    set_onChangeFunction($receiver_0_3.attrs, getCallableRef('onInputChange', function ($receiver, event) {
      return $receiver.onInputChange_9ojx7i$(event), Unit;
    }.bind(null, this)));
    $receiver_0_3.attrs.name = INPUT__FIELD__TITLE;
    $receiver_0_3.attrs.autoComplete = false;
    $receiver_0_3.attrs.placeholder = '\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435';
    $receiver_0_2.child_2usv9w$($receiver_0_3.create());
    var $receiver_0_4 = new RDOMBuilder_init(input$lambda(null, null, null, null, 'modal__input'));
    $receiver_0_4.attrs.type = InputType.text;
    set_onChangeFunction($receiver_0_4.attrs, getCallableRef('onInputChange', function ($receiver, event) {
      return $receiver.onInputChange_9ojx7i$(event), Unit;
    }.bind(null, this)));
    $receiver_0_4.attrs.name = INPUT__FIELD__DESCRIPTION;
    $receiver_0_4.attrs.autoComplete = false;
    $receiver_0_4.attrs.placeholder = '\u041E\u043F\u0438\u0441\u0430\u043D\u0438\u0435';
    $receiver_0_2.child_2usv9w$($receiver_0_4.create());
    $receiver_0_0.child_2usv9w$($receiver_0_2.create());
    var $receiver_0_5 = new RDOMBuilder_init(div$lambda_0('modal__button-list'));
    var $receiver_0_6 = new RDOMBuilder_init(button$lambda_0(null, null, null, 'modal__button modal__button--add'));
    $receiver_0_6.unaryPlus_pdl1vz$('Add');
    set_onClickFunction($receiver_0_6.attrs, NewTaskComponent$render$lambda$lambda$lambda$lambda$lambda(this));
    $receiver_0_6.attrs.disabled = equals(this.state.description, '') || equals(this.state.title, '');
    $receiver_0_5.child_2usv9w$($receiver_0_6.create());
    var $receiver_0_7 = new RDOMBuilder_init(button$lambda_0(null, null, null, 'modal__button modal__button--close'));
    $receiver_0_7.unaryPlus_pdl1vz$('Close');
    set_onClickFunction($receiver_0_7.attrs, NewTaskComponent$render$lambda$lambda$lambda$lambda$lambda_0(this));
    $receiver_0_5.child_2usv9w$($receiver_0_7.create());
    $receiver_0_0.child_2usv9w$($receiver_0_5.create());
    if (this.state.error) {
      var $receiver_0_8 = new RDOMBuilder_init(p$lambda_0(null));
      $receiver_0_8.unaryPlus_pdl1vz$('Error');
      $receiver_0_0.child_2usv9w$($receiver_0_8.create());
    }
    $receiver_0.child_2usv9w$($receiver_0_0.create());
    $receiver.child_2usv9w$($receiver_0.create());
  };
  function NewTaskComponent$onInputChange$lambda(closure$inputValue) {
    return function ($receiver) {
      $receiver.title = closure$inputValue;
      return Unit;
    };
  }
  function NewTaskComponent$onInputChange$lambda_0(closure$inputValue) {
    return function ($receiver) {
      $receiver.description = closure$inputValue;
      return Unit;
    };
  }
  function NewTaskComponent$onInputChange$lambda_1($receiver) {
    $receiver.error = false;
    return Unit;
  }
  function NewTaskComponent$onInputChange$lambda_2($receiver) {
    $receiver.error = true;
    return Unit;
  }
  NewTaskComponent.prototype.onInputChange_9ojx7i$ = function (event) {
    var tmp$;
    var target = Kotlin.isType(tmp$ = event.target, HTMLInputElement) ? tmp$ : throwCCE();
    var inputValue = target.value;
    if (!equals(target.name, '')) {
      switch (target.name) {
        case 'title':
          setState(this, NewTaskComponent$onInputChange$lambda(inputValue));
          break;
        case 'description':
          setState(this, NewTaskComponent$onInputChange$lambda_0(inputValue));
          break;
      }
      setState(this, NewTaskComponent$onInputChange$lambda_1);
    }
     else {
      setState(this, NewTaskComponent$onInputChange$lambda_2);
    }
  };
  NewTaskComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'NewTaskComponent',
    interfaces: [RComponent]
  };
  function Task(name, description) {
    this.name = name;
    this.description = description;
    this.isCompleted = false;
  }
  Task.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Task',
    interfaces: []
  };
  Task.prototype.component1 = function () {
    return this.name;
  };
  Task.prototype.component2 = function () {
    return this.description;
  };
  Task.prototype.copy_puj7f4$ = function (name, description) {
    return new Task(name === void 0 ? this.name : name, description === void 0 ? this.description : description);
  };
  Task.prototype.toString = function () {
    return 'Task(name=' + Kotlin.toString(this.name) + (', description=' + Kotlin.toString(this.description)) + ')';
  };
  Task.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.name) | 0;
    result = result * 31 + Kotlin.hashCode(this.description) | 0;
    return result;
  };
  Task.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.name, other.name) && Kotlin.equals(this.description, other.description)))));
  };
  function ToDoListProps() {
  }
  ToDoListProps.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ToDoListProps',
    interfaces: []
  };
  function ToDoList() {
    RComponent_init(this);
  }
  var LI_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.LI;
  function li$lambda(closure$classes) {
    return function (it) {
      return new LI_init(attributesMapOf('class', closure$classes), it);
    };
  }
  var UL_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.UL;
  function ul$lambda(closure$classes) {
    return function (it) {
      return new UL_init(attributesMapOf('class', closure$classes), it);
    };
  }
  function div$lambda_1(closure$classes) {
    return function (it) {
      return new DIV_init(attributesMapOf('class', closure$classes), it);
    };
  }
  ToDoList.prototype.render_ss14n$ = function ($receiver) {
    var $receiver_0 = new RDOMBuilder_init(div$lambda_1('task-list'));
    var $receiver_0_0 = new RDOMBuilder_init(ul$lambda(null));
    var tmp$;
    var $receiver_1 = this.props.taskList;
    if ($receiver_1 == null || $receiver_1.isEmpty()) {
      var $receiver_0_1 = new RDOMBuilder_init(li$lambda(null));
      $receiver_0_1.unaryPlus_pdl1vz$('There are no tasks yet...');
      $receiver_0_0.child_2usv9w$($receiver_0_1.create());
    }
     else {
      tmp$ = this.props.taskList.iterator();
      while (tmp$.hasNext()) {
        var task = tmp$.next();
        card($receiver_0_0, task, this.props.checkTask);
      }
    }
    $receiver_0.child_2usv9w$($receiver_0_0.create());
    $receiver.child_2usv9w$($receiver_0.create());
  };
  ToDoList.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ToDoList',
    interfaces: [RComponent]
  };
  function card$lambda$lambda$lambda(closure$checkTask, closure$task) {
    return function (it) {
      var tmp$;
      var target = Kotlin.isType(tmp$ = it.target, HTMLInputElement) ? tmp$ : throwCCE();
      closure$checkTask(closure$task, target.checked);
      return Unit;
    };
  }
  var H3_init = $module$kotlin_react_dom.$$importsForInline$$['kotlinx-html-js'].kotlinx.html.H3;
  function h3$lambda(closure$classes) {
    return function (it) {
      return new H3_init(attributesMapOf('class', closure$classes), it);
    };
  }
  function p$lambda_1(closure$classes) {
    return function (it) {
      return new P_init(attributesMapOf('class', closure$classes), it);
    };
  }
  function input$lambda_0(closure$type, closure$formEncType, closure$formMethod, closure$name, closure$classes) {
    return function (it) {
      return new INPUT_init(attributesMapOf_0(['type', closure$type != null ? enumEncode(closure$type) : null, 'formenctype', closure$formEncType != null ? enumEncode(closure$formEncType) : null, 'formmethod', closure$formMethod != null ? enumEncode(closure$formMethod) : null, 'name', closure$name, 'class', closure$classes]), it);
    };
  }
  function li$lambda_0(closure$classes) {
    return function (it) {
      return new LI_init(attributesMapOf('class', closure$classes), it);
    };
  }
  function card($receiver, task, checkTask) {
    var $receiver_0 = new RDOMBuilder_init(li$lambda_0('card'));
    if (task.isCompleted) {
      set_classes($receiver_0.attrs, setOf('card card--completed'));
    }
    var $receiver_0_0 = new RDOMBuilder_init(h3$lambda('card__title'));
    $receiver_0_0.unaryPlus_pdl1vz$(task.name);
    $receiver_0.child_2usv9w$($receiver_0_0.create());
    var $receiver_0_1 = new RDOMBuilder_init(p$lambda_1('card__description'));
    $receiver_0_1.unaryPlus_pdl1vz$(task.description);
    $receiver_0.child_2usv9w$($receiver_0_1.create());
    var $receiver_0_2 = new RDOMBuilder_init(input$lambda_0(null, null, null, null, 'card__input'));
    $receiver_0_2.attrs.type = InputType.checkBox;
    $receiver_0_2.attrs.checked = task.isCompleted;
    set_onChangeFunction($receiver_0_2.attrs, card$lambda$lambda$lambda(checkTask, task));
    $receiver_0.child_2usv9w$($receiver_0_2.create());
    $receiver.child_2usv9w$($receiver_0.create());
  }
  var INPUT__FIELD__TITLE;
  var INPUT__FIELD__DESCRIPTION;
  function main$lambda$lambda($receiver) {
    return Unit;
  }
  function main$lambda($receiver) {
    $receiver.child_drr62v$(getKClass(App), main$lambda$lambda);
    return Unit;
  }
  function main(args) {
    var rootDiv = document.getElementById('root');
    render(rootDiv, main$lambda);
  }
  Object.defineProperty(App, 'Companion', {
    get: App$Companion_getInstance
  });
  _.App = App;
  _.HomeViewState = HomeViewState;
  $$importsForInline$$['kotlin-react-dom'] = $module$kotlin_react_dom;
  _.HomeView = HomeView;
  _.NewTaskComponentProps = NewTaskComponentProps;
  _.NewTaskComponentState = NewTaskComponentState;
  _.NewTaskComponent = NewTaskComponent;
  _.Task = Task;
  _.ToDoListProps = ToDoListProps;
  _.ToDoList = ToDoList;
  _.card_rpy91h$ = card;
  Object.defineProperty(_, 'INPUT__FIELD__TITLE', {
    get: function () {
      return INPUT__FIELD__TITLE;
    }
  });
  Object.defineProperty(_, 'INPUT__FIELD__DESCRIPTION', {
    get: function () {
      return INPUT__FIELD__DESCRIPTION;
    }
  });
  _.main_kand9s$ = main;
  INPUT__FIELD__TITLE = 'title';
  INPUT__FIELD__DESCRIPTION = 'description';
  main([]);
  Kotlin.defineModule('test', _);
  return _;
}(module.exports, require('kotlin'), require('kotlin-react'), require('kotlin-react-dom'), require('kotlinx-html-js')));

//# sourceMappingURL=test.js.map
