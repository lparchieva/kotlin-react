import kotlinx.html.InputType
import kotlinx.html.classes
import kotlinx.html.js.onChangeFunction
import org.w3c.dom.HTMLInputElement
import react.RBuilder
import react.dom.*

// пример функционального компонента
fun RBuilder.card(task: Task, checkTask: (task: Task, checked: Boolean) -> Unit) {
    li("card") {
        if (task.isCompleted) {
            attrs.classes = setOf("card card--completed")
        }
        h3("card__title") {
            +task.name
        }
        p("card__description") {+task.description}
        input(classes ="card__input") {
            attrs.type = InputType.checkBox
            attrs.checked = task.isCompleted
            attrs.onChangeFunction = {
                val target = it.target as HTMLInputElement
                checkTask(task, target.checked)
            }
        }
    }
}