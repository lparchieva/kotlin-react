import react.*
import react.dom.div

class App:RComponent<RProps, RState>() {

    companion object {
        const val TO_DO_APP = "TO_DO_APP"
    }

    override fun RBuilder.render() {
        child(HomeView::class){}
    }
}