import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.*


interface ToDoListProps : RProps {
    var taskList: MutableList<Task>
    var checkTask: (task: Task, checked: Boolean) -> Unit
}

class ToDoList: RComponent<ToDoListProps, RState>() {
    override fun RBuilder.render() {
        div("task-list") {
            ul {
                if (props.taskList.isNullOrEmpty()) {
                    li {+"There are no tasks yet..."}
                } else {
                    for (task in props.taskList) {
                        card(task, props.checkTask)
                    }
                }
            }
        }
    }
}