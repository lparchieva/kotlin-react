import App.Companion.TO_DO_APP
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.*

interface HomeViewState : RState {
    var loading: Boolean
    var taskList: MutableList<Task>
    var createModalOpened: Boolean
}

class HomeView : RComponent<RProps, HomeViewState>() {

    override fun HomeViewState.init() {
        loading = false
        taskList = mutableListOf(Task("Пример", "Описание"))
    }

    private fun closeModal() {
        setState{
            createModalOpened = false
        }
    }

    private fun addToTaskList(task: Task) {
        val newTaskList = state.taskList
        newTaskList.add(task)
        setState { taskList = newTaskList }
    }

    private fun checkTask(task: Task, checked: Boolean) {
        val newTaskList = state.taskList
        newTaskList.find {task == it}?.let { it.isCompleted = checked }
        setState {
            taskList = newTaskList
        }
    }

    override fun RBuilder.render() {
        div {
            header("header") {
                h1("layout header__title"){
                    +"Simple React + Kotlin TODO App"
                }
            }
            div("main layout") {
                if (state.loading) {
                    p { +"Loading"}
                } else {
                    div("main__list") {
                        child(ToDoList::class){
                            attrs.taskList = state.taskList
                            attrs.checkTask = ::checkTask
                        }
                    }

                }
                aside("main__controls") {
                    button(classes = "main__button") {
                        +"ADD"
                        attrs.onClickFunction = {
                            setState{
                                createModalOpened = true
                            }
                        }
                    }
                }
            }

            if (state.createModalOpened) {
                child(NewTaskComponent::class){
                    attrs.onAdd = { closeModal() }
                    attrs.Add = ::addToTaskList
                }
            }
        }
    }
}