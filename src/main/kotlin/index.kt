import react.dom.*
import kotlin.browser.document

fun main(args: Array<String>) {
    val rootDiv = document.getElementById("root")

    render(rootDiv) {
        // how you call class component
        child(App::class){}
    }
}
