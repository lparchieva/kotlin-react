import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.events.Event
import react.*
import react.dom.*

interface NewTaskComponentProps : RProps {
    var onAdd: () -> Unit
    var Add: (task: Task) -> Unit
}

interface NewTaskComponentState : RState {
    var title: String
    var description: String
    var error: Boolean
}

class NewTaskComponent: RComponent<NewTaskComponentProps, NewTaskComponentState>() {
    override fun NewTaskComponentState.init() {
        title = ""
        description = ""
        error = false
    }

    override fun RBuilder.render() {
        div("modal") {
            div("modal__content") {
                h1 {
                    +"Добавить дело"
                }
                div("modal__input-list") {
                    input(classes = "modal__input") {
                        attrs.type = InputType.text
                        attrs.onChangeFunction = ::onInputChange
                        attrs.name = INPUT__FIELD__TITLE
                        attrs.autoComplete = false
                        attrs.placeholder = "Название"
                    }
                    input(classes = "modal__input") {
                        attrs.type = InputType.text
                        attrs.onChangeFunction = ::onInputChange
                        attrs.name = INPUT__FIELD__DESCRIPTION
                        attrs.autoComplete = false
                        attrs.placeholder = "Описание"
                    }
                }
                div("modal__button-list") {
                    button(classes = "modal__button modal__button--add") {
                        +"Add"
                        attrs.onClickFunction = {
                            props.Add(Task(state.title, state.description))
                            setState {
                                title = ""
                                description = ""
                            }
                            props.onAdd()
                        }
                        attrs.disabled = state.description == "" || state.title == ""
                    }
                    button(classes = "modal__button modal__button--close") {
                        +"Close"
                        attrs.onClickFunction = {
                            props.onAdd()
                        }
                    }
                }
                if (state.error) {
                    p {+"Error"}
                }
            }

        }
    }

    fun onInputChange(event: Event) {
        val target = event.target as HTMLInputElement
        val inputValue = target.value
        if (target.name != "") {
            when(target.name) {
                INPUT__FIELD__TITLE -> setState{ title = inputValue}
                INPUT__FIELD__DESCRIPTION -> setState{ description = inputValue}
            }
            setState{ error = false }
        } else {
            setState { error = true }
        }
    }
}
